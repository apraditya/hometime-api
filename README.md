# README

This is Ruby on Rails engineer skill assessment for Hometime. It contains one
API endpoint accepting several payloads mentioned in the intruction.

## Setup

- Clone this repository and go to the folder
- Run `bundle`
- Run `rails db:setup`
- Run `rails s` to run the app
- Run `rspec` to run all the test suite.

