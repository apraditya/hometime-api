# t.string :first_name
# t.string :last_name
# t.string :email
# t.string :phone

require 'rails_helper'

RSpec.describe Guest, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :email }
    it { is_expected.to validate_uniqueness_of(:email) }
    it { is_expected.to validate_presence_of :first_name }
  end
end
