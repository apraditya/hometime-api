# t.integer :guest_id, index: true
# t.string :code
# t.date :start_date
# t.date :end_date
# t.string :status
# t.integer :total_nights
# t.integer :total_guests
# t.integer :total_adults
# t.integer :total_children
# t.string :currency
# t.float :payout_price
# t.float :security_price
# t.float :total_price

require 'rails_helper'

RSpec.describe Reservation, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :code }
    it { is_expected.to validate_uniqueness_of(:code) }
  end
end
