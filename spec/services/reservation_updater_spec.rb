require 'rails_helper'

RSpec.describe ReservationUpdater do
  let(:payload_dir) { Rails.root.join('spec/factories/payloads') }
  let(:payload) { JSON.parse(File.read(payload_dir.join(payload_file))) }

  describe 'returns reservation attributes from airbnb payload' do
    let(:payload_file) { 'airbnb.com.json' }
    let(:rsv_attrs) { ReservationParser.call(payload) }

    subject { ReservationUpdater.call(payload) }

    it "creates a guest if it doesn't exist" do
      expect { subject }.to change { Guest.count }.to(1)
    end

    it "creates a reservation if it doesn't exist" do
      expect { subject }.to change { Reservation.count }.to(1)
    end

    it 'the created reservation belongs to the new guest' do
      subject
      guest = Guest.last
      rsv = Reservation.last
      expect(rsv.guest_id).to eq(guest.id)
    end

    describe 'when the guest exist' do
      before do
        @guest = FactoryBot.create(:guest, email: payload.dig('guest', 'email'))
      end

      it 'creates a reservation for the guest' do
        expect { subject }.to change { Reservation.count }.to(1)
        rsv = Reservation.last
        expect(rsv.guest_id).to eq(@guest.id)
      end

      describe 'and the reservation' do
        before do
          total_guests = rsv_attrs['total_guests'] + 1
          total_adults = rsv_attrs['total_adults'] + 1
          @reservation = FactoryBot.create(
            :reservation,
            rsv_attrs.merge('total_guests' => total_guests, 'total_adults' => total_adults)
          )
        end

        it 'updates the reservation detail' do
          expect { subject }.to change { @reservation.reload.total_guests }.by(-1)
        end
      end
    end
  end

  describe "when the payload isn't supported" do
    let(:payload) { Hash[] }

    subject { ReservationUpdater.call(payload) }

    it 'returns false' do
      expect(subject).to eq(false)
    end
  end
end
