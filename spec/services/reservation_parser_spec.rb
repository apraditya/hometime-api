require 'rails_helper'

RSpec.shared_examples 'getting the reservation attributes' do
  let(:guest_email) { 'guest@email.com' }
  let(:expected) { Hash[] }

  subject { ReservationParser.call(payload) }

  it 'has code and other info' do
    attrs = subject
    expect(attrs['code']).to eq(expected[:rsv_code])
    expect(attrs['status']).to eq(expected[:rsv_status])
    expect(attrs['total_guests']).to eq(expected[:total_guests])
    expect(attrs['total_adults']).to eq(expected[:total_adults])
    expect(attrs['total_children']).to eq(expected[:total_children])
  end

  it 'has period details' do
    attrs = subject
    expect(attrs['start_date']).to eq(expected[:start_date])
    expect(attrs['end_date']).to eq(expected[:end_date])
    expect(attrs['total_nights']).to eq(expected[:total_nights])
  end

  it 'has price details' do
    attrs = subject
    expect(attrs['currency']).to eq(expected[:currency])
    expect(attrs['payout_price']).to eq(expected[:payout_price])
    expect(attrs['security_price']).to eq(expected[:security_price])
    expect(attrs['total_price']).to eq(expected[:total_price])
  end

  describe "when the guest isn't registered" do
    it 'the guest_id is nil' do
      attrs = subject
      expect(attrs['guest_id']).to eq(nil)
    end

    it 'returns guest attributes to populate' do
      attrs = subject
      expect(attrs['guest_attrs']).to match(expected[:guest_attrs])
    end
  end

  describe "when the guest is registered" do
    before do
      @guest = FactoryBot.create(:guest, email: guest_email)
    end

    it 'the guest_id is the guest id' do
      attrs = subject
      expect(attrs['guest_id']).to eq(@guest.id)
    end

    it 'the guest attributes is nil' do
      attrs = subject
      expect(attrs['guest_attrs']).to eq(nil)
    end
  end
end

RSpec.describe ReservationParser do
  let(:payload_dir) { Rails.root.join('spec/factories/payloads') }
  let(:payload) { JSON.parse(File.read(payload_dir.join(payload_file))) }

  describe 'returns reservation attributes from airbnb payload' do
    it_behaves_like 'getting the reservation attributes' do
      let(:payload_file) { 'airbnb.com.json' }
      let(:guest_email) { payload.dig('guest', 'email') }
      let(:expected) do
        {
          rsv_code: payload['reservation_code'],
          rsv_status: payload['status'],
          total_guests: payload['guests'],
          total_adults: payload['adults'],
          total_children: payload['children'],
          start_date: payload['start_date'],
          end_date: payload['end_date'],
          total_nights: payload['nights'],
          currency: payload['currency'],
          payout_price: payload['payout_price'],
          security_price: payload['security_price'],
          total_price: payload['total_price'],
          guest_attrs: payload['guest']
        }
      end
    end
  end

  describe 'returns reservation attributes from booking.com payload' do
    it_behaves_like 'getting the reservation attributes' do
      let(:payload_file) { 'booking.com.json' }
      let(:guest_email) { payload.dig('reservation', 'guest_email') }
      let(:expected) do
        rsv_hash = payload['reservation']
        guest_hash = rsv_hash['guest_details']
        {
          rsv_code: rsv_hash['code'],
          rsv_status: rsv_hash['status_type'],
          total_guests: rsv_hash['number_of_guests'],
          total_adults: guest_hash['number_of_adults'],
          total_children: guest_hash['number_of_children'],
          start_date: rsv_hash['start_date'],
          end_date: rsv_hash['end_date'],
          total_nights: rsv_hash['nights'],
          currency: rsv_hash['host_currency'],
          payout_price: rsv_hash['expected_payout_amount'],
          security_price: rsv_hash['listing_security_price_accurate'],
          total_price: rsv_hash['total_paid_amount_accurate'],
          guest_attrs: {
            'first_name' => rsv_hash['guest_first_name'],
            'last_name' => rsv_hash['guest_last_name'],
            'phone' => rsv_hash['guest_phone_numbers'].first,
            'email' => rsv_hash['guest_email']
          }
        }
      end
    end
  end

  describe "when the payload isn't supported" do
    let(:payload) { Hash[] }

    subject { ReservationParser.call(payload) }

    it 'returns empty hash' do
      attrs = subject
      expect(attrs).to be_empty
    end
  end
end
