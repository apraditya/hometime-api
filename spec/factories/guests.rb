FactoryBot.define do
  factory :guest do
    first_name { "James" }
    last_name { "Hetfield" }
    email { "jh@example.com" }
    phone { "639123456789" }
  end
end
