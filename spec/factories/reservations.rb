FactoryBot.define do
  factory :reservation do
    code { "YYY12345678" }
    start_date { "2021-10-18".to_date }
    end_date { "2021-10-20".to_date }
    status { "accepted" }
    total_nights { 2 }
    total_guests { 3 }
    total_adults { 2 }
    total_children { 1 }
    currency { "AUD" }
    payout_price { 2300.00 }
    security_price { 200.00 }
    total_price { 2500.00 }
  end
end
