require 'rails_helper'

RSpec.describe ReservationsController, type: :controller do
  describe "'POST' webhook" do
    subject { post :webhook, params: {} }

    describe 'update successful' do
      before do
        allow(ReservationUpdater).to receive(:new) do
          -> { true }
        end
      end

      it 'returns a correct status code' do
        subject
        expect(response.status).to eq(201)
      end
    end

    describe 'update failed' do
      before do
        allow(ReservationUpdater).to receive(:new) do
          -> { false }
        end
      end

      it 'returns a correct status code' do
        subject
        expect(response.status).to eq(400)
      end
    end

    describe "when an error is raised" do
      before do
        allow(ReservationUpdater).to receive(:new) do
          -> { raise 'An error is occurred' }
        end
      end

      it 'returns a correct status code' do
        subject
        expect(response.status).to eq(500)
      end
    end
  end
end
