class ReservationParser < ApplicationService
  attr_reader :payload

  def initialize(payload)
    @payload = payload
  end

  def call
    return {} unless payload_provider

    send("parse_#{payload_provider}")
  end

  private

  def payload_from_booking?
    rsv_hash = payload['reservation']
    return false unless rsv_hash

    !rsv_hash.dig('code').nil? && !rsv_hash.dig('guest_details').nil?
  end

  def payload_from_airbnb?
    !payload['reservation_code'].nil? && !payload.dig('guest', 'email').nil?
  end

  def payload_provider
    @payload_provider ||= [
      'booking',
      'airbnb'
    ].find { |name| send("payload_from_#{name}?") }
  end

  def parse_airbnb
    attrs = payload.slice(
      'start_date',
      'end_date',
      'status',
      'currency',
      'payout_price',
      'security_price',
      'total_price'
    )
    attrs['code'] = payload['reservation_code']
    attrs['total_nights'] = payload['nights']
    attrs['total_guests'] = payload['guests']
    attrs['total_adults'] = payload['adults']
    attrs['total_children'] = payload['children']

    guest_attrs = payload['guest']
    guest = Guest.find_by email: guest_attrs['email']
    attrs['guest_id'] = guest&.id
    attrs['guest_attrs'] = guest_attrs unless guest

    attrs
  end

  def parse_booking
    rsv_hash = payload['reservation']
    attrs = rsv_hash.slice('code', 'start_date', 'end_date')
    attrs['status'] = rsv_hash['status_type']
    attrs['total_nights'] = rsv_hash['nights']
    attrs['total_guests'] = rsv_hash['number_of_guests']

    guest_hash = rsv_hash['guest_details']
    attrs['total_adults'] = guest_hash['number_of_adults']
    attrs['total_children'] = guest_hash['number_of_children']
    attrs['currency'] = rsv_hash['host_currency']
    attrs['payout_price'] = rsv_hash['expected_payout_amount']
    attrs['security_price'] = rsv_hash['listing_security_price_accurate']
    attrs['total_price'] = rsv_hash['total_paid_amount_accurate']

    guest_email = rsv_hash['guest_email']
    guest = Guest.find_by email: guest_email
    attrs['guest_id'] = guest&.id
    unless guest
      attrs['guest_attrs'] = {
        'first_name' => rsv_hash['guest_first_name'],
        'last_name' => rsv_hash['guest_last_name'],
        'phone' => rsv_hash['guest_phone_numbers'].first,
        'email' => guest_email
      }
    end

    attrs
  end
end
