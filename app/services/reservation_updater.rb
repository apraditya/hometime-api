class ReservationUpdater < ApplicationService
  attr_reader :payload

  def initialize(payload)
    @payload = payload
  end

  def call
    rsv_attrs = ReservationParser.call(payload)
    return false if rsv_attrs.empty?

    reservation = Reservation.find_by code: rsv_attrs['code']

    if reservation
      reservation.update rsv_attrs
    elsif rsv_attrs['guest_id']
      reservation = Reservation.new rsv_attrs
      reservation.save
    else
      guest = Guest.new rsv_attrs.delete('guest_attrs')
      guest.reservations.build rsv_attrs
      guest.save
    end
  end
end
