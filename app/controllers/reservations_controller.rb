class ReservationsController < ApplicationController
  def webhook
    if ReservationUpdater.call(params)
      head 201
    else
      head 400
    end
  rescue
    head 500
  end
end
