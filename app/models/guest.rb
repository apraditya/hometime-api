# t.string :first_name
# t.string :last_name
# t.string :email
# t.string :phone

class Guest < ApplicationRecord
  has_many :reservations

  validates :email, presence: true, uniqueness: true
  validates :first_name, presence: true
end
